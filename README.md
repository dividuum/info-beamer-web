# This repository is no longer maintained and kept for historical reasons
Please have a look at the [new documentation](https://info-beamer.com/doc/) instead.

---

info-beamer documentation.

See [info-beamer.org](http://info-beamer.org/) or gh-pages branch.
